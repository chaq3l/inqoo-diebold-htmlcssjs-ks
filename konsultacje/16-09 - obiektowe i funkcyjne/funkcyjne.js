// Funckje zamiast wartosci

function sum(x, y) { return x + y }
function mult(x, y) { return x * y }
function sub(x, y) { return x - y }

var result = sum(1, 2)
result = mult(result, 2)
result = sub(100, result)

// lub:

var result = sub(100, // 3
    mult(  // 2
        sum(1, 2), // 1
        2)
)

/* Funkcja jako parametr */

function obliczIzwroc(dane) { return dane }
function wyswietl(dane) { console.log(dane) }

wyswietl(obliczIzwroc([1, 2, 3]))

// Przekazywanie funkcji (kontunuacji CSP - continuation passing programing)
function obliczIprzekazDalej(dane, fn) { fn(dane) }

obliczIprzekazDalej([1, 2, 3], wyswietl)



/* 
=== Wywołania zwrotne - Callback 
*/

function zrobCosZaraz(param, callback) {
    setTimeout(function () {
        wynik = param + 'timeout'
        console.log('operacja ' + wynik)
        callback(wynik)
    }, 1000)
}

// var result = zrobCosZaraz();
// console.log('wynik ' + result) // ''wynik undefined'

function wyswielt(dane) { console.log(dane) }

zrobCosZaraz('HEllo ', wyswielt)

//  operacja HEllo timeout
//  HEllo timeout


/* 
=== Zdarzenia - dodajemy 'potem' do callback'a jako parametr
*/
button = {
    type:'button',
    clickedTimes: 0,
    click: function(){
        this.clickedTimes++
        if(this.onclick) {
            var event = {type:'click', target: this}
            this.onclick(event)
        }
    },
    onclick: null
}

// Czy byl juz klikniety?

// if( button.clicked ){ ...
// if( button.clicked ){ ...
// if( button.clicked ){ ...
// ...

button.onclick = function(event) { 
    console.log(event.target, 'KLikniety ' + event.target.clickedTimes + ' razy' )
 } 

// ... pozniej ....

button.click()
button.click()
button.click()

// {type: 'button', clickedTimes: 1, click: ƒ, onclick: ƒ} 'KLikniety 1 razy'
// {type: 'button', clickedTimes: 2, click: ƒ, onclick: ƒ} 'KLikniety 2 razy'
// {type: 'button', clickedTimes: 3, click: ƒ, onclick: ƒ} 'KLikniety 3 razy'




/* 
=== Petle a funkcje:
 */

let myArray = [1, 2, 3, 4, 5, 6, 7, 8, 9]

function forEach(array, fn) {
    for (var i = 0; i < array.length; i++) {
        fn(array[i]);
    }
}

var x = typeof console.log(undefined) // uruchamiam i dostaje 'undefined'
var y = typeof console.log  // nie uruchamiam i mam nadal 'function'

forEach(myArray, console.log);


function sum(numbers) {
    debugger
    var total = 0;

    // var sum  = function (x) {
    //     total += x;
    // }

    // window.forEach(numbers, sum);
    window.forEach(numbers, function (x) {
        total += x;
    });
    /// lub wbudowana w Tablice:
    numbers.forEach(sum);

    return total;
}

console.log(sum([1, 10, 100]));


console.log([1, 10, 100].reduce((sum, x) => { return sum + x }), 0)
console.log([1, 10, 100].reduce((sum, x) => sum + x, 0))

// Prototyp Array:

// https://developer.mozilla.org/pl/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce#polyfill
function myReduce(items, fn, total) {
    for (let key in items) {
        total = fn(total, items[key], key, items)
    }
    return total
}
myReduce([1, 10, 100], (sum, x, index, numbers) => { return sum + x }, 0)


/// Using functions inside Objects - as Prototype

Array.prototype.myReduce = function myReduce(fn, total) {
    for (let key in this) {
        if (!this.hasOwnProperty(key)) break;
        total = fn(total, this[key], key, this)
    }
    return total
};

['x', 'y', 'z'].myReduce((sum, x) => sum + x, ' placki ')