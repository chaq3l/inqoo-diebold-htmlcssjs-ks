class ProductEditorView extends EventTarget{
    constructor(selector){
        super()

        this.el = document.querySelector(selector)
        this.form = this.el.querySelector('form')

        this.el.addEventListener('submit', (event) => {
            event.preventDefault()
            //console.log(this.getData())
            const data = this.getData()
            fetch('http://localhost:3000/products/' + this._data.id, {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            }).then(res => res.json())
            //.then(this.dispatchEvent(new SaveProductData(this.getData())))
            //.then(productListView.renderSingleProductOnList(this.getData()))
            .then(productList.filterJSON(''))
            

            
        })
        
    }
    /**
     *  @type Product
     * @protected
     */
   _data = {}

   setData(product){

       this._data = product
       this.form.elements['name'].value = this._data['name']
       this.form.elements['price'].value = this._data['price']
       this.form.elements['imagePath'].value = this._data['img']

   }

   getData(){
       this._data['name'] = this.form.elements['name'].value 
       this._data['price'] = Number(this.form.elements['price'].value)
       this._data['img'] = this.form.elements['imagePath'].value 
       return this._data
   }
   defineListViewListener(productList){
    productList.addEventListener('FillProductEditorForm', event => this.setData(event.product))
   }
}

var productEditorView = new ProductEditorView('#product-editor')
productEditorView.defineListViewListener(productListView)