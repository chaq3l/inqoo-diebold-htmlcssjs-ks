class NewProductAdded extends Event{
    constructor(product){
        super('NewProductAdded')
        this.product = product
    }
}

class ProductsFiltered extends Event{
    constructor(products){
        super('ProductsFiltered')
        this.products = products
    }
}

class FillProductEditorForm extends Event{
    constructor(product){
        super('FillProductEditorForm')
        this.product = product
    }
}

class SaveProductData extends Event{
    constructor(data){
        super('SaveProductData')
        this.data = data
    }
}
