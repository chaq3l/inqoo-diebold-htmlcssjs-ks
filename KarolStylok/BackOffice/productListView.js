class ProductListView extends EventTarget {
    constructor(selector) {
      super()
        this.el = document.querySelector(selector)
        this.el.addEventListener('click', (event)=> { 
          var parent = event.currentTarget
          parent.querySelectorAll('[data-product-id]').forEach(element => element.style.backgroundColor = "white" )
          var target = event.target.closest('[data-product-id]')          
            target.style.backgroundColor = "cyan" 
            
            var productId = target.dataset.productId      
            productList.findProductById(productId).then( product => this.dispatchEvent(new FillProductEditorForm(product))  )
               
        })
      }

      /**
   * 
   * @param {Product} product 
   */
    defineListViewListener(productList){
      productList.addEventListener('NewProductAdded', event => this.addProduct(event.product))
      productList.addEventListener('ProductsFiltered', event => this.filterProductList(event.products))      
  }
  defineProductEditorViewListener(productEditorView){
    productEditorView.addEventListener('SaveProductData', event => {
      this.restartProductList()
      this.renderProduct(event.data)
    })
  }

  restartProductList(){
  //  console.log(this.el)
   // this.el = `<div class="list-group" id="productList"> </div>`
 //   console.log(this.el.firstChild)
    while (this.el.firstChild) {
      this.el.removeChild(this.el.firstChild);
    }
  }

  filterProductList(products){
    this.restartProductList()
    for (var product of products){
      this.addProduct(product)
    }
  }

  renderSingleProductOnList(product){
    this.restartProductList()
    this. addProduct(product)
  }

  addProduct(product) {
    const div = document.createElement('div')
    div.innerHTML = this.renderProduct(product);    
    this.el.appendChild(div)

  }

  renderProduct(product) {
    return /* html */`<div class="list-group-product d-flex justify-content-between"  data-product-id="${product.id}">
        <div class="d-flex flex-column">
          <h3>${product.name}</h3>
          <p>.description</p>
        </div>
        <div class="d-flex flex-column">
            <img src="${product.img}" width="40%" height="auto">
        </div>
      <div class="d-flex-column">
        <div class="text-center">${product.price.toFixed(2)} USD</div>
        <div class="button btn btn-danger text-nowrap">Delete</div>
        </div>
    </div>`;   
  }

}
var productListView = new ProductListView('#productList')
productListView.defineListViewListener(productList)
//productListView.defineProductEditorViewListener(productEditorView)
productListView.restartProductList()