class ProductList extends EventTarget{
    constructor(){
        super()
        this._ProductItems = []     

    }

    addProduct(product) {
        let item = this._ProductItems.find(item => item.id === product.id)       
        if (item) {          
            console.log("product already exist")         
        } else {            
            this._ProductItems.push(product)
            this.dispatchEvent(new NewProductAdded(product))

        }        
      }

    filterProducts(name){
        const products = this._ProductItems.filter(product => product.name.toLocaleLowerCase().includes(name.toLocaleLowerCase()))        
        this.dispatchEvent(new ProductsFiltered(products))
    }

    response(res, query){
        this._data = res   
        this._results = this._data.filter(product => product.name.toLocaleLowerCase().includes(query.toLocaleLowerCase()))
        this._ProductItems = this._results        
        if(this._results)
        this.dispatchEvent(new ProductsFiltered(this._results))
    }

    getResponseById(res, id){        
        this._data = res
        // this._results = this._data.filter(product => product.id === id)    
        return this._data
    }
    
    filterJSONByID(query){        
            const resp = this.loadJSON('http://localhost:3000/products/' + query)                    
            
            return resp.then(res => { return this.getResponseById(res, query)})      
    }
    
    filterJSON(query){
        //this.loadJSON('products.json', resp=> {
            const resp = this.loadJSON('http://localhost:3000/products?q=' + query)            
            resp.then(res => {this.response(res, query)})
    }    

    loadJSON(url){
        return new Promise((resolve) => {
            const xhr = new XMLHttpRequest()
            xhr.open('GET', url)
            xhr.addEventListener('loadend', event => { 
            const data= JSON.parse(event.target.responseText)
            //console.log(data)
            resolve(data)
            })

        //xhr.addEventListener('error')
        xhr.send()
        })
    }

    findProductById(id){
        //const product = this._ProductItems.find(function(x){ return x.id == id }, 0)        
        return this.filterJSONByID(id)
    }
}


var productList = new ProductList()
function addTestProducts(){
    productList.addProduct(new Product(1, "Produkt1", "https://www.cocaflora.com/userdata/public/gfx/7109/Carmona.jpg", 125))
    productList.addProduct(new Product(2, "Produkt2", "https://www.cocaflora.com/userdata/public/gfx/7111/Carmona---przepiekna-40cm.jpg", 125))
    productList.addProduct(new Product(22, "Produkt22", "https://www.cocaflora.com/userdata/public/gfx/7109/Carmona.jpg", 125))
    productList.addProduct(new Product(12, "Produkt12", "https://www.cocaflora.com/userdata/public/gfx/7111/Carmona---przepiekna-40cm.jpg", 125))
    productList.addProduct(new Product(13, "Produkt13", "https://www.cocaflora.com/userdata/public/gfx/7109/Carmona.jpg", 125))
}
