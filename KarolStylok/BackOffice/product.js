class AbstractProduct {}

class Product extends AbstractProduct{
    constructor(id, name, img, price){
        super()
        this.id = id
        this.name = name
        this.img = img
        this.price = price
    }    
}