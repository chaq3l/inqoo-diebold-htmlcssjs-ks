var loan = 
    {
        nominal_interest: 0.1,
        ammount: 1000,
        installments: 12
    }



    var calculateRatesButton = document.getElementById('calculateRatesButton')
    calculateRatesButton.addEventListener('click', calculateLoanDetails)
    //calculateLoanDetails(loan)
    function calculateLoanDetails(){
        loan.ammount = document.getElementById('loan_amount').value
        loan.installments = document.getElementById('loan_months').value
        //loan.ammount= loan.ammount
        var elementToDisplay = ``
        var payment = 0
        var indebtedness = []  
        var capital_payment = []   
        var rate = []
        var interest = []
        var ammount = loan.ammount   
        //tbody.createElement()
        //var tbody = document.getElementById('loanTable')
        var tbody = document.querySelector('#payments-table tbody')
        tbody.innerHTML = ``    
       
        for(var i = 0; i < loan.installments; i++){
            ammount = Number(ammount) -  Number(payment)
            indebtedness.push(ammount)
            //console.log(dept)
            capital_payment[i] = loan.ammount/loan.installments
            interest.push(ammount*loan.nominal_interest/12)
            payment = capital_payment[i]
            rate.push(capital_payment[i] + interest[i])
            //console.log(String(i)+ " "+ String(indebtedness[i])+  " "+ String(capital_payment[i]+ " "+ String(interest[i])+ " "+ rate[i]))
            var tr = document.createElement('tr')
            tr.innerHTML= `            
            <th>${i+1}</th>
            <th>${indebtedness[i].toFixed(2)}</th>
            <th>${interest[i].toFixed(2)}</th>
            <th>${capital_payment[i].toFixed(2)}</th>
            <th>${rate[i].toFixed(2)}</th>
            `
            
            tbody.appendChild(tr)
        }
        var totalInterest = 0
        var total_capital_payment = 0
        var totalRate = 0
        for(var i = 0; i < indebtedness.length; i++){
            totalInterest += interest[i]
            total_capital_payment += capital_payment[i]
            totalRate += rate[i]
        }
        var tr = document.createElement('tr')
        tr.innerHTML= `        
        <th></th>
        <th>Sum:</th>
        <th>${totalInterest.toFixed(2)}</th>
        <th>${total_capital_payment.toFixed(2)}</th>
        <th>${totalRate.toFixed(2)}</th>        
        `
        //console.log(elementToDisplay)
        tbody.appendChild(tr)
    }


