//import {products} from "./index"

var cartItems = [
    {
        product_id: '123',
        product: products[0],
        ammount: 2,
        subtotal: 2*Number(products[0].price)
        
    },



]

function resetCartItems(){
    cartItems = []
}
//console.log(cartItems)
renderCartHTMLElements(cartItems)

function deleteCartElementById(itemIdToDelete){
    
    const item = cartItems.find(item => item.product_id === itemIdToDelete.toString()) 
    
   if(item.ammount<=1){
       var cartElementFiltered = cartItems.filter(item => item.product_id !== itemIdToDelete.toString())
       cartItems = cartElementFiltered
   }else{
       item.ammount -=1
       item.subtotal = item.ammount * item.product.price
   }

    renderCartHTMLElements(cartItems)
}

function addToCart(id){
    const product = products.find(product => product.id == id)
    const sameItemCart =  cartItems.find(item => item.product_id == id)
    //console.log(sameItemCart)
    if(sameItemCart===undefined){
        const ammount = 1
        cartItems.push({
            product_id: String(product.id),
            product,
            ammount,
            subtotal: product.price
        })
    }else{
        sameItemCart.ammount += 1,
        sameItemCart.subtotal = sameItemCart.ammount*product.price
    }
    
    renderCartHTMLElements(cartItems)
}

function getTotal(cartItems){
    var ammountToPay = 0
    for (let product of cartItems) {       
        ammountToPay += Number(product.subtotal)/100
    }
    return ammountToPay
}

function renderCartHTMLElements(cartItems){
    var ammountToPay = getTotal(cartItems)
    var elementToRender = ''
    for (let product of cartItems) {
        elementToRender += renderSingleCartElementToDocumentById(product.product_id)        
    }
    cartListTotal.innerHTML= `
    <li class="list-group-item d-flex justify-content-between">
                <span>Total (PLN)</span>
                <strong>${ammountToPay.toFixed(2)}</strong>
              </li>
    `
    //console.log(elementToRender)
    cartList.innerHTML = elementToRender
}

function renderSingleCartElementToDocumentById(id){
    const product = cartItems.find(item => item.product_id == id)
    var price = product.subtotal/100
    // var item = `<li data-item-id="${product.product_id}"
    // class="list-group-item js-product d-flex justify-content-between lh-sm">
    //   <div>
    //     <h6 class="my-0">${product.product.name}</h6>
    //     <small class="text-muted">${product.product.description}</small>
    //   </div>
    //   <span class="text-muted">
    //   ${product.ammount} &times; ${product.product.price/100} = ${price}
    //   </span>
    //   <span class="close" onclick="deleteCartElementById(${product.product_id.toString()})">&times;</span>
    // </li>`

    var item = `
    <li class="list-group-item d-flex justify-content-between lh-sm">
    <div class="button btn btn-danger text-nowrap" onclick="deleteCartElementById(${product.product_id.toString()})">X</div>
    <div>                  
      <h6 class="my-0">${product.product.name}</h6>
      <small class="text-muted">${product.product.description}</small>
    </div>
    <span class="text-muted">${price.toFixed(2)}</span>
  </li>
  `
  return item
}
