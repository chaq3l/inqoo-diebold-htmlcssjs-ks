class CartView{
    constructor(selector, cart){
        this.el = document.querySelector(selector)
        this.connectCart(cart)
    }

    connectCart(cart) {
        cart.addEventListener('ItemAdded', event => this.addCartElementToView(event.item))
        cart.addEventListener('ItemRemoved', event => this.deleteCartElement(event.item))
        cart.addEventListener('ItemUpdated', event => this.updateCartElement(event.item))
    }
    //console.log(cartItems)
    //renderCartHTMLElements(cartItems)

    addCartElementToView(item){
        const div = document.createElement('div')
        div.innerHTML = this.renderSingleCartElement(item)
        const li = div.firstElementChild
        this.el.appendChild(li)
    } 

    deleteCartElement(item){        
        const itemEl = this.el.querySelector(`[data-item-id="${item.product_id}"]`)
        itemEl.remove()        
    }

    updateCartElement(item){
        const itemEl = this.el.querySelector(`[data-item-id="${item.product_id}"]`)
        itemEl.outerHTML = this.renderSingleCartElement(item)
    }        
            
    renderSingleCartElement(item){
        
    var price = item.subtotal/100
    return `<li data-item-id="${item.product_id}"
    class="list-group-item js-product d-flex justify-content-between lh-sm">
      <div>
        <h6 class="my-0">${item.product.name}</h6>
        <small class="text-muted">${item.product.description}</small>
      </div>
      <span class="text-muted">
      ${item.amount} &times; ${item.product.price/100} = ${price}
      </span>
      <span class="close" onclick="cart.removeProduct(${item.product_id.toString()})">&times;</span>
    </li>`
      
    }
}
var cartView = new CartView('#cartList', cart)