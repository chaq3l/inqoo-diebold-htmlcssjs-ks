resetCartItems()

addToCart('123')
addToCart('124')
addToCart('125')

console.assert(getTotal(cartItems) === ((1589+1689+1789)/100), 'Bad total sum')
resetCartItems()
console.log("End of getTotal test")