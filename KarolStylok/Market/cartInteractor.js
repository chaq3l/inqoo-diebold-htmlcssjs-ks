class Cart extends EventTarget {

  constructor() {
    super()

    /** @type AbstractCartItem[] */
    this._cartItems = []
    this._cartTotal = 0
  }

  reset() {
    this._cartItems = []
    this._cartTotal = 0
  }

  addProduct(product_id) {
    let item = this._cartItems.find(item => item.product_id === product_id.toString())

    if (item) {
      item.increment()
      this.dispatchEvent(new ItemUpdatedEvent(item))
    } else {
      const product = this.products.getById(product_id)
      
      item = new CartProduct(product)
      this._cartItems.push(item)
      this.dispatchEvent(new ItemAddedEvent(item))
    }
    this._cartTotal += item.product.price
  }

  getTotal() {
    return this._cartItems.reduce((sum, item) => sum + item.subtotal, 0)
  }

  removeProduct(product_id) {
    const item = this._cartItems.find(item => item.product_id === product_id.toString())
    //console.log(item)
    if (item) {
      try {
        item.decrement()
        this.dispatchEvent(new ItemUpdatedEvent(item))
      } catch (error) {
        this._cartItems = this._cartItems.filter(i => i.product_id !== item.product_id)
        this.dispatchEvent(new ItemRemovedEvent(item))
      }
      this._cartTotal -= item.product.price
    }
  }

  getItems() {
    return this._cartItems
  }
}

class ItemAddedEvent extends Event {
    constructor(item) {
      super('ItemAdded')
      this.item = item
    }
  }
  class ItemRemovedEvent extends Event {
    constructor(item) {
      super('ItemRemoved')
      this.item = item
    }
  }
  class ItemUpdatedEvent extends Event {
    constructor(item) {
      super('ItemUpdated')
      this.item = item
    }
  }
  
  
  class Products {
  
    /**
     * Collection of Products 
     * @param {Product[]} products 
     */
    constructor(products) {
      this._products = products
    }
  
    /**
     * Find product By Id
     * @param {string} product_id 
     * @returns Product
     */
    getById(product_id) {
      return this._products.find(product => product.id === product_id.toString())
    }
  }
  
  var cart = new Cart()
  cart.products = new Products(window.products)
  
  // window.basket = new Cart()
  
  // makeCart(window.cart = {})
  // makeCart(window.basket = {})
  
  
  /**  @type Product */
      // var prod = productsGetById('123')
      // prod.name.toUpperCase()
  
  /**
   * @typedef Product
   * @property {string} id
   * @property {string} name
   * @property {number} price Nett price in cents
   */
  
  /**
   * @typedef CartItem
   * @property {string} product_id
   * @property {Product} product
   * @property {number} amount
   * @property {number} subtotal
   */
  