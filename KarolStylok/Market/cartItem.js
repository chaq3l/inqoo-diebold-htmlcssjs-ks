class AbstractCartItem{

}

class CartProduct extends AbstractCartItem{
    constructor(product, amount = 1){
        super()
        this.product = product
        this.product_id = product.id
        this._updateAmount(amount)        
    }
    _updateAmount(amount){
        if(amount<=0) {throw new Error('Item can not be zero or lower than zero')}
        this.amount = amount
        this.subtotal = amount*Number(this.product.price)
    }
    increment(){
        this._updateAmount(this.amount+1)
    }
    decrement(){
        this._updateAmount(this.amount-1)
    }
}

class CartDiscount extends AbstractCartItem{

}