/**
 * @typedef Task
 * @property {string} id
 * @property {string} title
 * @property {boolean} completed
 */


export class ProductsCollection {

  /** @type Task[] */
  _data = [
    {
      "id": "123",
      "title": "Kup placki",
      "completed": true
    },
    {
      "id": "234",
      "title": "Kup Mleko",
      "completed": false
    },
  ]

  /** 
   * 
   * @returns {Task[]} tasks 
   */
  getAll() {
    return this._data
  }

  getById() { }

}