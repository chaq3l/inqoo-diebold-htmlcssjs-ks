import { ProductsCollection } from './ProductsCollection.js'
class ProductsView {

  collection = new ProductsCollection()
  el = document.querySelector('#tasks-list')

  render() {

    this.collection.getAll().forEach(task => {

      const div = document.createElement('div')
      div.classList.add('list-group-item')
      div.dataset.taskId = task.id

      /* es6-string-html - wtyczka */
      div.innerHTML = /* html */`
        <input type="checkbox" checked=${task.completed} />
        <span>${task.title}</span>
      `
      this.el.appendChild(div)
    })

  }
}

const productsView = new ProductsView()
productsView.render()

window.productsView = productsView